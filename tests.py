import unittest
from main import app
import json

def get_exam0():
    return {
        "baseline_value": 150,
        "accelerations": 0.01,
        "fetal_movement": 0.01,
        "uterine_contractions": 0.01,
        "light_decelerations": 0.01,
        "severe_decelerations": 0.01,
        "prolongued_decelerations": 0.01,
        "abnormal_short_term_variability": 70,
        "mean_value_of_short_term_variability": 70,
        "percentage_of_time_with_abnormal_long_term_variability": 70,
        "mean_value_of_long_term_variability": 70,
        "histogram_width": 70,
        "histogram_min": 30,
        "histogram_max": 80,
        "histogram_number_of_peaks": 70,
        "histogram_number_of_zeroes": 70,
        "histogram_mode": 70,
        "histogram_mean": 60,
        "histogram_median": 60,
        "histogram_variance": 10,
        "histogram_tendency": 70
    }


class Tests(unittest.TestCase):
    def setUp(self) -> None:
        self.app_client = app.test_client()

    def test_api(self):
        response = self.app_client.post('/autoexam', json=get_exam0())
        result = json.loads(response.data.decode('utf-8')).get('result')
        self.assertEqual(result, 1.)


if __name__ == '__main__':
    unittest.main()
