from flask import Flask, request, jsonify

from examanalyzer import ExamAnalyzer as EA
from util import from_dict, get_fields

app = Flask(__name__)
ea = EA()


@app.route('/autoexam', methods=['POST'])
def autoexam():
    msgexam = request.get_json()
    ctgexam = dict([(k, msgexam[k]) for k in get_fields()])

    exam = from_dict(ctgexam)
    result = ea.analyser_exam(ctgexam=exam)
    return jsonify({
        "result": result[0]
    })


if __name__ == '__main__':
    app.run(debug=True)
